#!/bin/bash

echo 'Server = http://arch.mithril.re/$repo/os/$arch' > /etc/pacman.d/mirrorlist

pacman-key --init
pacman-key --populate archlinux
pacman -Sy gnupg archlinux-keyring --noconfirm
pacman-key --refresh-keys
pacman -Syu --noconfirm

ln -sf /usr/share/zoneinfo/Indian/Reunion /etc/localtime

echo "LC_ALL=fr_FR.UTF-8" >> /etc/environment
echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen
echo "LANG=fr_FR.UTF-8" >> /etc/locale.conf
locale-gen fr_FR.UTF-8

echo 'PasswordAuthentication no' >> /etc/ssh/sshd_config
systemctl enable --now sshd

pacman -S vim htop bash-completion git --noconfirm
